#!/usr/bin/bash

result=0
for((i=1;i<=5;i++))
do
    j=0
    while((j+i<=5))
    do        
        j=$((j + i))
        result=$((result ^ 2**(j-1)))                       
    done    
    out=$(echo "obase=2;$result" | bc)
    printf "%05d\n" $out | rev

done
